package com.demo.csjbot.csjsdkdemo.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.bean.FAKbean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KnowledgeActivity extends AppCompatActivity {

    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledge);
        ButterKnife.bind(this);


        FAKbean faKbean = (FAKbean) getIntent().getSerializableExtra("knowledge");
        webView.setWebViewClient(new WebViewClient());
//        String mRichText="<p style=\"text-align: left;\"><img src=\"https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1383902695,1129447956&fm=26&gp=0.jpg\"\n" +
//                "\t title=\"1541054054758015328.jpg\" alt=\"1.jpg\" /><img src=\"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2353449440,2528668120&fm=26&gp=0.jpg\"\n" +
//                "\t title=\"1541054057414099008.jpg\" alt=\"2.jpg\" /><img src=\"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3446458559,1680525880&fm=26&gp=0.jpg\"\n" +
//                "\t title=\"1541054060899024343.jpg\" alt=\"3.jpg\" /></p>";
        webView.loadDataWithBaseURL(null,getHtmlData(faKbean.getContent()), "text/html" , "utf-8", null);
        //webView.loadUrl(faKbean.getUrl());
    }
    private String getHtmlData(String bodyHTML) {
        String head = "<head>"
                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"> "
                + "<style>img{max-width: 100%; width:auto; height:auto;}</style>"
                + "</head>";
        bodyHTML=bodyHTML.replace("./WebRoot","http://101.200.128.17:7080/firstaid/WebRoot");
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()){
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
