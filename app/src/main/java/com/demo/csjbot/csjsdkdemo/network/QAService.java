package com.demo.csjbot.csjsdkdemo.network;

import com.demo.csjbot.csjsdkdemo.bean.QAbean;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Author: Ge Chen
 * Date: 2019/11/8 10:14
 * Description:
 */
public interface QAService {
    /*
     *EmergencyInquirySystem/qa/get_next_question?userInput=xxx
     */
    @GET("EmergencyInquirySystem/qa/get_next_question?appId=20191113")
    Observable<QAbean> getAnswer(@Query("userInput") String userInput);


    //http://101.200.128.17:7080/QASystem/cxf/test/get/你好/testuser?appId=20191113
    @GET("QASystem/cxf/test/get/{question}/testuser?appId=20191113")
    Observable<ResponseBody> getAnswer2(@Path("question")String question);
}
