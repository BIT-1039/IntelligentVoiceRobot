package com.demo.csjbot.csjsdkdemo.sdkpack;

import android.content.Context;
import android.util.Log;

import com.csjbot.coshandler.core.CsjRobot;
import com.demo.csjbot.csjsdkdemo.base.MyApplication;
import com.robot.performlib.action.AIUIAction;
import com.robot.performlib.action.SpeakAction;

/**
 * Author: Ge Chen
 * Date: 2019/10/28 13:27
 * Description: 语音类实现
 */
public class Voice {
    private static final String TAG = "Voice";

    static CsjRobot csjRobot;

    static SpeakAction speakAction;


    public static void speak(Context context, String text) {
        if (MyApplication.SDK_TYPE == 0) {
            speakAction = SpeakAction.getInstance();
            if (speakAction != null) {
                speakAction.speak(context, text);
            } else {
                Log.d(TAG, "YunJi is not connect!");
            }
        } else if (MyApplication.SDK_TYPE == 1) {
            csjRobot = CsjRobot.getInstance();
            if (csjRobot.getState().isConnect()) {
                csjRobot.getTts().startSpeaking(text, null);
            } else {
                Log.d(TAG, "csjRobot is not connect! ");
            }
        }
    }

    public static void stop(Context context) {
        speakAction.pauseSpeaking(context);
    }

    public static void resume(Context context) {
        speakAction.resumeSpeaking(context);
    }

    public static void changeVoicer(Context context, AIUIAction.Voicer voicer, int speed, int intonation) {
        AIUIAction.changeVoicer(context, voicer, speed, intonation);
    }


}