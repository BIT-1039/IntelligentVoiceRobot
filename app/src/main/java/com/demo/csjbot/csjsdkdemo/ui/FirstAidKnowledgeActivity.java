package com.demo.csjbot.csjsdkdemo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.bean.FAKbean;
import com.demo.csjbot.csjsdkdemo.network.KnowledgeService;
import com.demo.csjbot.csjsdkdemo.network.URL;
import com.demo.csjbot.csjsdkdemo.util.FAKAdapter;
import com.demo.csjbot.csjsdkdemo.util.LoadMoreListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FirstAidKnowledgeActivity extends AppCompatActivity {
    @BindView(R.id.rv_fa_knowledge)
    RecyclerView recyclerView;
    @BindView(R.id.swiperefreshlayout2)
    SwipeRefreshLayout swipeRefreshLayout;

    private FAKAdapter myAdapter;
    private ArrayList<FAKbean> data;

    KnowledgeService knowledgeService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_aid_knowledge);

        ButterKnife.bind(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefreshlayout2);
        //下拉刷新
        swipeRefreshLayout.setOnRefreshListener(() -> {
            Toast.makeText(FirstAidKnowledgeActivity.this, "刷新", Toast.LENGTH_SHORT).show();
            swipeRefreshLayout.setRefreshing(false);
        });

        initData();
        initRecyclerView();
    }

    private void initData() {
        data = new ArrayList<>();
        //数据不够多时 可能会不能覆盖屏幕 导致footer错误出现
        knowledgeService = URL.retrofit.create(KnowledgeService.class);
        knowledgeService.getKnowledgeList("","",0,10)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<FAKbean>>(){
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(FirstAidKnowledgeActivity.this, "加载错误", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<FAKbean> fakbeans) {
                        // Toast.makeText(FirstAidVideoActivity.this, "333", Toast.LENGTH_SHORT).show();
                        data.addAll(fakbeans);
                        myAdapter.updateList(null, false);
                    }
                });
    }

    private void initRecyclerView() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new FAKAdapter(FirstAidKnowledgeActivity.this, data);
        recyclerView.setAdapter(myAdapter);

        //点击跳转
        myAdapter.setOnItemClickListener(position -> {
            Toast.makeText(FirstAidKnowledgeActivity.this, "点击第" + position + "个", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(FirstAidKnowledgeActivity.this, KnowledgeActivity.class).putExtra("knowledge", data.get(position)));
        });

        //下拉刷新
        swipeRefreshLayout.setOnRefreshListener(() -> {
            Toast.makeText(FirstAidKnowledgeActivity.this, "刷新", Toast.LENGTH_SHORT).show();
            swipeRefreshLayout.setRefreshing(false);
        });

        //上拉加载
        recyclerView.addOnScrollListener(new LoadMoreListener() {
            @Override
            protected void onLoading() {
                Toast.makeText(FirstAidKnowledgeActivity.this, "加载", Toast.LENGTH_SHORT).show();
                myAdapter.updateList(null, false);
            }
        });
    }
}
