package com.demo.csjbot.csjsdkdemo.bean;

import java.util.List;

/**
 * Author: Ge Chen
 * Date: 2019/11/8 10:30
 * Description:
 */
public class QAbean {
    private String text;
    private List<Media> media;
    private Integer timeout;
    private String voice;
    private Integer type;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Media> getMedia() {
        return media;
    }

    public void setMedia(List<Media> media) {
        this.media = media;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "QAbean{" +
                "text='" + text + '\'' +
                ", media=" + media +
                ", timeout=" + timeout +
                ", voice='" + voice + '\'' +
                ", type=" + type +
                '}';
    }
}
