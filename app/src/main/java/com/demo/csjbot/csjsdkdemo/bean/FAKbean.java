package com.demo.csjbot.csjsdkdemo.bean;

import java.io.Serializable;

public class FAKbean implements Serializable {
    private int id;
    private String title;
    private String url;
    private String category;
    private String content;
    private String introduction;
    private long date;

    public FAKbean(){

    }
    public FAKbean(int id, String title, String url, String category, String content, String introduction, long date) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.category = category;
        this.content = content;
        this.introduction = introduction;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
