package com.demo.csjbot.csjsdkdemo.util;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.DrawableRes;


/**
 * Author: Ge Chen
 * Date: 2019/11/7 16:35
 * Description:
 */
public class UriUtil {
    public static Uri getUri(Activity activity, @DrawableRes int resId) {
        return Uri.parse("res://" + activity.getApplication().getPackageName() + "/" + resId);
    }
}
