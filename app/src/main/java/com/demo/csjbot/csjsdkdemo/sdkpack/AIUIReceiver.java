package com.demo.csjbot.csjsdkdemo.sdkpack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.base.MyApplication;
import com.demo.csjbot.csjsdkdemo.ui.IntelligentQAActivity;
import com.demo.csjbot.csjsdkdemo.util.UriUtil;
import com.hotelrobot.common.constant.AIUIConstant;

/**
 * Author: Ge Chen
 * Date: 2019/10/28 13:31
 * Description: 云迹全局广播接收器
 */
public class AIUIReceiver extends BroadcastReceiver {
    private final String shutDown = "shutdown";
    private static String iat;


    @Override
    public void onReceive(final Context context, Intent intent) {
        if (intent == null || MyApplication.SDK_TYPE != 0)
            return;
        if (MyApplication.QA_CLOSED) {
            return;
        }

        if (TextUtils.equals(intent.getAction(), "YUNFAN_SPEAKING")) {
            String type = "";
            if (intent.hasExtra(AIUIConstant.Key.type))
                type = intent.getStringExtra(AIUIConstant.Key.type);

        } else if (TextUtils.equals(intent.getAction(), "BROADCAST_TEST")) {
            String type = "";
            String content = "";
            String sintent = "";

            if (intent.hasExtra(AIUIConstant.Key.type))
                type = intent.getStringExtra(AIUIConstant.Key.type);

            if (intent.hasExtra(AIUIConstant.Key.content))
                content = intent.getStringExtra(AIUIConstant.Key.content);

            if (intent.hasExtra(AIUIConstant.Key.intent))
                sintent = intent.getStringExtra(AIUIConstant.Key.intent);

            if (TextUtils.isEmpty(type)) {
                return;
            }
            switch (type) {
                case AIUIConstant.Type.wakeUp://唤醒
                    MyApplication.WAKE_UP_STATUS = true;
                    IntelligentQAActivity.setAndSpeak("重新开始");
                    MyApplication.getmActivity().pauseAndResume.setImageURI(UriUtil.getUri(MyApplication.getmActivity(), R.drawable.pause));
                    MyApplication.getmActivity().isPaused = false;
                    break;
                case AIUIConstant.Type.Iat://语音识别结果
                    iat = content;
                    if (!TextUtils.isEmpty(iat) && MyApplication.recognizing ) {
                        IntelligentQAActivity.setAndSpeak(iat);
                    } else {
                        iat = "";
                    }
                    break;
                case AIUIConstant.Type.speakBegin:
                    String speakTag_Begin = "";
                    //speakTag是调用speak方法是传递的标记值，说话开始前回调时会把这个值再返回，用以区分
                    if (intent.hasExtra(AIUIConstant.Key.speakTag)) {
                        MyApplication.recognizing = false;
                    }
                    break;
                case AIUIConstant.Type.speakCompleted:
                    String speakTag = "";
                    //speakTag是调用speak方法是传递的标记值，说话完成回调时会把这个值再返回，用以区分
                    if (intent.hasExtra(AIUIConstant.Key.speakTag)) {
                        speakTag = intent.getStringExtra(AIUIConstant.Key.speakTag);
                        //例如：接收到tag为shutdown的语音播报完成，执行屏幕断电动作
                        if (TextUtils.equals(shutDown, speakTag)) {
                            Log.d("GeChen", "onReceive: ");
                        }
                        iat = "";
                        //MyApplication.recognizing = true;
                        if(MyApplication.videoQA!=null){
                            MyApplication.videoQA.start();
                        }else if(!MyApplication.choosing){
                            MyApplication.recognizing = true;
                        }
                    }
                    break;
            }
        }
    }

}
