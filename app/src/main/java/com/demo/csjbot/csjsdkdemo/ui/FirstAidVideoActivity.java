package com.demo.csjbot.csjsdkdemo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.bean.FAVbean;
import com.demo.csjbot.csjsdkdemo.network.URL;
import com.demo.csjbot.csjsdkdemo.network.VideoService;
import com.demo.csjbot.csjsdkdemo.util.FAVAdapter;
import com.demo.csjbot.csjsdkdemo.util.LoadMoreListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FirstAidVideoActivity extends AppCompatActivity {

    @BindView(R.id.rv_fa_video)
    RecyclerView recyclerView;
    @BindView(R.id.swiperefreshlayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private FAVAdapter myAdapter;
    private ArrayList<FAVbean> data;
    VideoService videoService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_aid_video);

        ButterKnife.bind(this);

        //下拉刷新
        swipeRefreshLayout.setOnRefreshListener(() -> {
            Toast.makeText(FirstAidVideoActivity.this, "刷新", Toast.LENGTH_SHORT).show();
            swipeRefreshLayout.setRefreshing(false);
        });

        initData();
        initRecyclerView();
    }

    private void initData() {
        data = new ArrayList<>();
        /*for (int i = 0; i < 10; i++) {
            FAVbean x = new FAVbean(i, i + "", "测试", "https://flv2.bn.netease.com/videolib1/1811/26/OqJAZ893T/HD/OqJAZ893T-mobile.mp4","","",0);
            data.add(x);
        }*/

        videoService = URL.retrofit.create(VideoService.class);
        videoService.getVideoList("视频","",0,10)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<FAVbean>>(){
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(FirstAidVideoActivity.this, "加载错误", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<FAVbean> faVbeans) {
                       // Toast.makeText(FirstAidVideoActivity.this, "333", Toast.LENGTH_SHORT).show();
                        data.addAll(faVbeans);
                        myAdapter.updateList(null, false);
                    }
                });

        //数据不够多时 可能会不能覆盖屏幕 导致footer错误出现
    }

    private void initRecyclerView() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new FAVAdapter(FirstAidVideoActivity.this, data);
        recyclerView.setAdapter(myAdapter);

        //点击跳转
        myAdapter.setOnItemClickListener(position -> {
            Toast.makeText(FirstAidVideoActivity.this, "点击第" + position + "个", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(FirstAidVideoActivity.this, VideoActivity.class).putExtra("video", data.get(position)));
        });

        //上拉加载
        recyclerView.addOnScrollListener(new LoadMoreListener() {
            @Override
            protected void onLoading() {
                Toast.makeText(FirstAidVideoActivity.this, "加载", Toast.LENGTH_SHORT).show();
                /*new Handler().postDelayed(() -> {
                    ArrayList<FAVbean> newDatas = new ArrayList<>();
                    newDatas.add(new FAVbean(data.size(), data.size() + "", "测试", "https://flv2.bn.netease.com/videolib1/1811/26/OqJAZ893T/HD/OqJAZ893T-mobile.mp4","","",0));
                    newDatas.add(new FAVbean(data.size()+1, data.size()+1 + "", "测试", "https://flv2.bn.netease.com/videolib1/1811/26/OqJAZ893T/HD/OqJAZ893T-mobile.mp4","","",0));

                    myAdapter.updateList(newDatas, true);
                }, 500);*/

                myAdapter.updateList(null, false);
            }
        });
    }
}
