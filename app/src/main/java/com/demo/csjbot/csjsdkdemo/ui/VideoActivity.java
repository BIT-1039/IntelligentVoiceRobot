package com.demo.csjbot.csjsdkdemo.ui;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.bean.FAVbean;
import com.demo.csjbot.csjsdkdemo.util.CustomVideoView;
import com.demo.csjbot.csjsdkdemo.util.FAVAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Author : GeChen
 * Date : 2019/11/15 12:31
 * Description :
 */
public class VideoActivity extends AppCompatActivity {
    @BindView(R.id.video)
    CustomVideoView videoView;

    @BindView(R.id.video_title)
    TextView videoTitle;
    @BindView(R.id.video_desc)
    TextView videoDesc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this);
        initVideo();
    }

    private void initVideo() {
        MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        FAVbean video = (FAVbean) getIntent().getSerializableExtra("video");
        videoTitle.setText(video.getMediaName());
        videoDesc.setText(video.getMediaDescription());
        videoView.setVideoPath(video.getMediaPath());

        videoView.start();

        videoView.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
            @Override
            public void onPlay() {
                Toast.makeText(VideoActivity.this, "play", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPause() {
                Toast.makeText(VideoActivity.this, "pause+"+videoView.getCurrentPosition(), Toast.LENGTH_SHORT).show();

            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Toast.makeText(VideoActivity.this, "播放完成", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
