package com.demo.csjbot.csjsdkdemo.util;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.base.MyApplication;
import com.demo.csjbot.csjsdkdemo.bean.Media;
import com.demo.csjbot.csjsdkdemo.bean.QAbean;
import com.demo.csjbot.csjsdkdemo.ui.IntelligentQAActivity;
import com.facebook.drawee.view.SimpleDraweeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Author : GeChen
 * Date : 2019/12/2 14:49
 * Description :
 */
public class AnswerAdapter {
    Context context;
    int type;
    List<QAbean> qAbeanList=new ArrayList<>();

    public AnswerAdapter(Context context, int type) {
        this.type = type;
        this.context = context;
    }

    public List<QAbean> getqAbeanList() {
        return qAbeanList;
    }

    public void setqAbeanList(List<QAbean> qAbeanList) {
        this.qAbeanList = qAbeanList;
    }

    public static View getRelativeLayout(AnswerAdapter answerAdapter, QAbean qAbean) {
        MyApplication.videoQA=null;

        //问答 多个选项
        if (answerAdapter.type == 10){
            //处理成qabean list
            String string=qAbean.getText();
            JSONArray jsonArray = null;
            List<QAbean> qAbeanList=new ArrayList<>();
            List<String> choiceList=new ArrayList<>();
            try {
                jsonArray = new JSONArray(string);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    choiceList.add(jsonObject.getString("question"));
                    QAbean qAbean1=new QAbean();
                    qAbean1.setText(jsonObject.getString("answer")+"您还有需要帮助的吗?");
                    List<Media> mediaList=new ArrayList<>();
                    mediaList.add(qAbean.getMedia().get(i));
                    qAbean1.setMedia(mediaList);
                    qAbean1.setTimeout(qAbean.getTimeout());
                    qAbean1.setVoice(qAbean.getVoice());
                    if(qAbean1.getMedia().get(0)!=null){
                        if(qAbean1.getMedia().get(0).getType().equals("图片")){
                            qAbean1.setType(2);
                        }
                        if(qAbean1.getMedia().get(0).getType().equals("视频")){
                            qAbean1.setType(3);
                        }
                    }else{
                        qAbean1.setType(1);
                    }
                    qAbeanList.add(qAbean1);
                }
                if(jsonArray.length()>1){
                    View view = LayoutInflater.from(answerAdapter.context).inflate(R.layout.item_txt_and_rv, null, false);
                    TextView txt = view.findViewById(R.id.txt);
                    txt.setText("您问的哪种？请选择一项");
                    txt.setMovementMethod(ScrollingMovementMethod.getInstance());
                    RecyclerView recyclerView=view.findViewById(R.id.rv_choices);
                    recyclerView.setLayoutManager(new LinearLayoutManager(answerAdapter.context));
                    ChoiceAdapter myAdapter = new ChoiceAdapter(answerAdapter.context, choiceList);
                    recyclerView.setAdapter(myAdapter);
                    answerAdapter.setqAbeanList(qAbeanList);
                    myAdapter.setOnItemClickListener(position -> {
                        if(answerAdapter.listener!=null){
                            answerAdapter.listener.onClick(position);
                        }
                    });
                    return view;
                }
                answerAdapter.type=qAbeanList.get(0).getType();
                return getRelativeLayout(answerAdapter,qAbeanList.get(0));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (answerAdapter.type == 1) {
            View view = LayoutInflater.from(answerAdapter.context).inflate(R.layout.item_txt, null, false);
            TextView textView = view.findViewById(R.id.txt);
            textView.setText(qAbean.getText());
            textView.setMovementMethod(ScrollingMovementMethod.getInstance());
            return view;
        } else if (answerAdapter.type == 2) {
            View view = LayoutInflater.from(answerAdapter.context).inflate(R.layout.item_txt_and_pic, null, false);
            TextView textView = view.findViewById(R.id.txt);
            textView.setText(qAbean.getText());
            textView.setMovementMethod(ScrollingMovementMethod.getInstance());
            SimpleDraweeView simpleDraweeView = view.findViewById(R.id.pic);
            simpleDraweeView.setImageURI(qAbean.getMedia().get(0).getUrl());
            return view;
        } else if (answerAdapter.type == 3) {
            View view = LayoutInflater.from(answerAdapter.context).inflate(R.layout.item_txt_and_video, null, false);
            TextView textView = view.findViewById(R.id.txt);
            textView.setText(qAbean.getText());
            textView.setMovementMethod(ScrollingMovementMethod.getInstance());
            MyApplication.videoQA = (CustomVideoView)view.findViewById(R.id.video);
            MediaController mediaController = new MediaController(answerAdapter.context);

            //mediaController.setVisibility(View.GONE);
            MyApplication.videoQA.setMediaController(mediaController);
            MyApplication.videoQA.setVideoPath(qAbean.getMedia().get(0).getUrl());
            MyApplication.videoQA.setBackgroundColor(Color.TRANSPARENT);
            MyApplication.videoQA.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
                @Override
                public void onPlay() {

                }

                @Override
                public void onPause() {

                }
            });
            MyApplication.videoQA.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    MyApplication.recognizing=true;
                }
            });
           /* MyApplication.videoQA.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                            if(i==MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START)
                                MyApplication.videoQA.setBackgroundColor(Color.TRANSPARENT);
                            return true;
                        }
                    });
                }
            });*/
            return view;
        }
        return null;
    }

    //点击事件
    public interface OnItemClickListener {
        void onClick(int position);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
