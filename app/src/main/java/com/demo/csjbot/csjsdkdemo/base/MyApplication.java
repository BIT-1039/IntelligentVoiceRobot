package com.demo.csjbot.csjsdkdemo.base;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.csjbot.coshandler.core.CsjRobot;
import com.demo.csjbot.csjsdkdemo.ui.IntelligentQAActivity;
import com.demo.csjbot.csjsdkdemo.util.CustomVideoView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.hjq.bar.TitleBar;
import com.hjq.bar.style.TitleBarLightStyle;
import com.robot.performlib.action.SDKProp;

/**
 * Author: Ge Chen
 * Date: 2019/10/28 13:20
 * Description:
 */

public class MyApplication extends MultiDexApplication {
    private static Context context;


    //0-云迹SDK;1-穿山甲SDK
    public static int SDK_TYPE = 0;
    public static boolean WAKE_UP_STATUS = true;
    public static boolean QA_CLOSED = true;
    //正在识别
    public static boolean recognizing = true;
    public static boolean choosing = false;
    //视频准备播放
    public static CustomVideoView videoQA = null;


    private static IntelligentQAActivity mActivity;
    public static String answer = "初始化";

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        /**
         * 初始化云迹SDK
         */
        SDKProp.aiuiInit(this);

        /**
         * 初始化穿山甲SDK
         */
        CsjRobot.getInstance().init(this);

        /*
         * 初始化Fresco
         * */
        Fresco.initialize(this);

        TitleBar.initStyle(new TitleBarLightStyle(this));

    }

    public static IntelligentQAActivity getmActivity() {
        return mActivity;
    }

    public static void setmActivity(IntelligentQAActivity mActivity) {
        MyApplication.mActivity = mActivity;
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        MyApplication.context = context;
    }
}
