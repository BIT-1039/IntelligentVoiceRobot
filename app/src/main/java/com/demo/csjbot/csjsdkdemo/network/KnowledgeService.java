package com.demo.csjbot.csjsdkdemo.network;

import com.demo.csjbot.csjsdkdemo.bean.FAKbean;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface KnowledgeService {
    @GET("EmergencyInquirySystem/knowledge/getKnowledgeList")
    Observable<List<FAKbean>> getKnowledgeList(@Query("type") String type, @Query("search") String search, @Query("start") int start, @Query("num") int num);
}
