package com.demo.csjbot.csjsdkdemo.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.base.MyApplication;
import com.demo.csjbot.csjsdkdemo.bean.QAbean;
import com.demo.csjbot.csjsdkdemo.network.QAService;
import com.demo.csjbot.csjsdkdemo.network.URL;
import com.demo.csjbot.csjsdkdemo.sdkpack.Voice;
import com.demo.csjbot.csjsdkdemo.util.AnswerAdapter;
import com.demo.csjbot.csjsdkdemo.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Author: Ge Chen
 * Date: 2019/11/7 16:10
 * Description:智能问答
 */
public class IntelligentQAActivity extends AppCompatActivity {
    @BindView(R.id.chat_view)
    SimpleDraweeView chatView;
    @BindView(R.id.txt_question)
    public TextView textQuestion;
    @BindView(R.id.show_part)
    public RelativeLayout showPart;
    @BindView(R.id.pauseAndResume)
    public SimpleDraweeView pauseAndResume;

    //是否暂停
    public boolean isPaused = false;

    //类别 是急救问答 还是疾病诊断 1 2
    Integer type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intelligent_qa);
        type = getIntent().getIntExtra("type", 1);

        ButterKnife.bind(this);
        initParams();
        initView();
        initListener();

    }

    private void initParams() {
        MyApplication.QA_CLOSED = false;
        MyApplication.setmActivity(this);
        MyApplication.recognizing = true;
    }

    private void initListener() {
        pauseAndResume.setOnClickListener(view -> {
            if (!isPaused) {
                pauseAndResume.setImageURI(UriUtil.getUri(IntelligentQAActivity.this, R.drawable.resume));
                Voice.stop(IntelligentQAActivity.this);
                isPaused = true;
                if (MyApplication.videoQA != null && MyApplication.videoQA.isPlaying()) {
                    MyApplication.videoQA.pause();
                }
            } else {
                pauseAndResume.setImageURI(UriUtil.getUri(IntelligentQAActivity.this, R.drawable.pause));
                Voice.resume(IntelligentQAActivity.this);
                isPaused = false;
                if (MyApplication.videoQA != null && !MyApplication.videoQA.isPlaying() && MyApplication.videoQA.getCurrentPosition() > 500) {
                    MyApplication.videoQA.start();
                }
            }
        });
    }

    private void initView() {
        DraweeController draweeController1 = Fresco.newDraweeControllerBuilder().setUri(UriUtil.getUri(IntelligentQAActivity.this, R.drawable.chat))
                .build();
        chatView.setController(draweeController1);
        pauseAndResume.setImageURI(UriUtil.getUri(IntelligentQAActivity.this, R.drawable.pause));
    }

    public static void setAndSpeak(String question) {
        MyApplication.getmActivity().textQuestion.setText(question);
        if (MyApplication.getmActivity().type == 1) {
            QAService qaService = URL.retrofit.create(QAService.class);
            qaService.getAnswer(question)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<QAbean>() {
                        @Override
                        public void onCompleted() {
                            Voice.speak(MyApplication.getmActivity(), MyApplication.answer);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(MyApplication.getmActivity(), "错误" + e.toString(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNext(QAbean qAbean) {

                            AnswerAdapter answerAdapter = new AnswerAdapter(MyApplication.getmActivity(), qAbean.getType());
                            View view = AnswerAdapter.getRelativeLayout(answerAdapter, qAbean);
                            MyApplication.answer = qAbean.getText();
                            if (qAbean.getType() != 10) {
                                MyApplication.answer = qAbean.getText();
                                MyApplication.choosing = false;
                            } else {
                                MyApplication.answer = "您问的哪种？请选择一项";
                                MyApplication.choosing = true;
                            }
                            answerAdapter.setOnItemClickListener(position -> {
                                AnswerAdapter answerAdapter1 = new AnswerAdapter(MyApplication.getmActivity(), answerAdapter.getqAbeanList().get(position).getType());
                                View view1 = AnswerAdapter.getRelativeLayout(answerAdapter1, answerAdapter.getqAbeanList().get(position));
                                MyApplication.answer = answerAdapter.getqAbeanList().get(position).getText();
                                MyApplication.getmActivity().showPart.removeAllViews();
                                MyApplication.getmActivity().showPart.addView(view1);
                                Voice.speak(MyApplication.getmActivity(), MyApplication.answer);
                                MyApplication.choosing = false;
                            });
                            MyApplication.getmActivity().showPart.removeAllViews();
                            MyApplication.getmActivity().showPart.addView(view);
                        }
                    });
        } else if (MyApplication.getmActivity().type == 2) {
            QAService qaService = URL.retrofit.create(QAService.class);
            qaService.getAnswer2("你好")
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ResponseBody>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(MyApplication.getmActivity(), "错误" + e.toString(), Toast.LENGTH_SHORT).show();
                            System.out.println(e);
                        }

                        @Override
                        public void onNext(ResponseBody responseBody) {
                            /*try {
                                Toast.makeText(MyApplication.getmActivity(), responseBody.string(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }*/
                            QAbean qAbean=new QAbean();
                            qAbean.setType(1);
                            try {
                                qAbean.setText(responseBody.string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            AnswerAdapter answerAdapter = new AnswerAdapter(MyApplication.getmActivity(), 1);
                            View view = AnswerAdapter.getRelativeLayout(answerAdapter, qAbean);
                            MyApplication.answer = qAbean.getText();

                            MyApplication.getmActivity().showPart.removeAllViews();
                            MyApplication.getmActivity().showPart.addView(view);
                        }
                    });
        }

    }

    @Override
    protected void onStop() {
        if (MyApplication.recognizing = false) {
            Voice.stop(IntelligentQAActivity.this);
        }
        MyApplication.QA_CLOSED = true;
        super.onStop();
    }
}
