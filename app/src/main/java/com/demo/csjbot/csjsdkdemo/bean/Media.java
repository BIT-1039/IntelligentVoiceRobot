package com.demo.csjbot.csjsdkdemo.bean;

/**
 * Author : GeChen
 * Date : 2019/12/2 16:19
 * Description :
 */
public class Media {
    private String type;
    private String url;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Media{" +
                "type='" + type + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
