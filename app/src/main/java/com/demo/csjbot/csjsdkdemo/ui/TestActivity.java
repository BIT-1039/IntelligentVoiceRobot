package com.demo.csjbot.csjsdkdemo.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.base.MyApplication;
import com.demo.csjbot.csjsdkdemo.bean.QAbean;
import com.demo.csjbot.csjsdkdemo.network.QAService;
import com.demo.csjbot.csjsdkdemo.network.URL;
import com.demo.csjbot.csjsdkdemo.sdkpack.Voice;
import com.demo.csjbot.csjsdkdemo.util.AnswerAdapter;
import com.demo.csjbot.csjsdkdemo.util.ChoiceAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Author : GeChen
 * Date : 2019/12/2 15:25
 * Description :
 */
public class TestActivity extends AppCompatActivity {
    @BindView(R.id.relative)
    RelativeLayout relativeLayout;
    @BindView(R.id.test)
    Button test;
    @BindView(R.id.test2)
    Button test2;
    @BindView(R.id.test3)
    Button test3;
    @BindView(R.id.test4)
    Button test4;

    QAService qaService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* View view1 = LayoutInflater.from(TestActivity.this).inflate(R.layout.item_txt_and_pic, null);
                TextView textView = view1.findViewById(R.id.txt);
                textView.setText("测试");
                relativeLayout.addView(view1);*/

                /*View view1 = LayoutInflater.from(TestActivity.this).inflate(R.layout.item_txt_and_rv, null);
                TextView txt=view1.findViewById(R.id.txt);
                txt.setText("您问的哪种止血法？请选择一项。");
                RecyclerView recyclerView=view1.findViewById(R.id.rv_choices);
                recyclerView.setLayoutManager(new LinearLayoutManager(TestActivity.this));
                ArrayList<String> data= new ArrayList<>();
                data.add("头部止血法");
                data.add("下肢止血法");
                data.add("上肢止血法");


                ChoiceAdapter myAdapter = new ChoiceAdapter(TestActivity.this, data);
                recyclerView.setAdapter(myAdapter);

                myAdapter.setOnItemClickListener(position -> {
                    Toast.makeText(TestActivity.this, "点击第" + position + "个", Toast.LENGTH_SHORT).show();
                });
                relativeLayout.addView(view1);*/
                View view1 = LayoutInflater.from(TestActivity.this).inflate(R.layout.item_txt_scroll, null);
                TextView textView = view1.findViewById(R.id.txt);
                textView.setText("先别慌。立即拨打120，告诉有人发生心脏骤停，和你们所在位置。\t請按我說的去做：快跪在病人右侧，解开上衣，暴露胸部。把左手掌放在病人胸部中央，兩乳头连线水平。右手放在左手上，快速向下做心脏按压30次，要快速，用力。跟着我属数按压，1、2、3、4、5、6、7、8、9、10、11、12、13、14、15、16、17、18、19、20、21、22、23、24、25、26、27、28、29、30。请口对口人工呼吸，缓慢吹气1次， 再缓慢吹气1次，继续胸外按压，1、2、3、4、5、6、7、8、9、10、11、12、13、14、15、16、17、18、19、20、21、22、23、24、25、26、27、28、29、30。缓慢吹气1次， 再缓慢吹气1次，请连续再做3组。检查病人有反应吗？");
                textView.setMovementMethod(ScrollingMovementMethod.getInstance());
                relativeLayout.removeAllViews();
                relativeLayout.addView(view1);

            }
        });

        test2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View view1 = LayoutInflater.from(TestActivity.this).inflate(R.layout.item_txt_autosize, null);
                TextView textView = view1.findViewById(R.id.txt);
                textView.setText("先别慌。立即拨打120，告诉有人发生心脏骤停，和你们所在位置。\t請按我說的去做：快跪在病人右侧，解开上衣，暴露胸部。把左手掌放在病人胸部中央，兩乳头连线水平。右手放在左手上，快速向下做心脏按压30次，要快速，用力。跟着我属数按压，1、2、3、4、5、6、7、8、9、10、11、12、13、14、15、16、17、18、19、20、21、22、23、24、25、26、27、28、29、30。请口对口人工呼吸，缓慢吹气1次， 再缓慢吹气1次，继续胸外按压，1、2、3、4、5、6、7、8、9、10、11、12、13、14、15、16、17、18、19、20、21、22、23、24、25、26、27、28、29、30。缓慢吹气1次， 再缓慢吹气1次，请连续再做3组。检查病人有反应吗？");
                relativeLayout.removeAllViews();
                relativeLayout.addView(view1);
            }
        });
        test3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View view1 = LayoutInflater.from(TestActivity.this).inflate(R.layout.item_txt_autosize, null);
                TextView textView = view1.findViewById(R.id.txt);
                textView.setText("您好 我是急救机器人");
                relativeLayout.removeAllViews();
                relativeLayout.addView(view1);
            }
        });

        test4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qaService = URL.retrofit.create(QAService.class);
                qaService.getAnswer("止血法")
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<QAbean>() {
                            @Override
                            public void onCompleted() {
                                //Voice.speak(MyApplication.getmActivity(), MyApplication.answer);
                                Toast.makeText(TestActivity.this, "ok", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(TestActivity.this, "错误"+e.toString(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNext(QAbean qAbean) {
                                AnswerAdapter answerAdapter = new AnswerAdapter(TestActivity.this, qAbean.getType());
                                View view = AnswerAdapter.getRelativeLayout(answerAdapter, qAbean);
                                if(qAbean.getType()!=10) MyApplication.answer = qAbean.getText();
                                else MyApplication.answer = "您问的哪种？请选择一项";
                                answerAdapter.setOnItemClickListener(position -> {
                                    AnswerAdapter answerAdapter1 = new AnswerAdapter(TestActivity.this, answerAdapter.getqAbeanList().get(position).getType());
                                    View view1 = AnswerAdapter.getRelativeLayout(answerAdapter1, answerAdapter.getqAbeanList().get(position));
                                    MyApplication.answer = answerAdapter.getqAbeanList().get(position).getText();
                                    relativeLayout.removeAllViews();
                                    relativeLayout.addView(view1);
                                });
                                relativeLayout.removeAllViews();
                                relativeLayout.addView(view);
                            }
                        });
            }
        });
    }
}
