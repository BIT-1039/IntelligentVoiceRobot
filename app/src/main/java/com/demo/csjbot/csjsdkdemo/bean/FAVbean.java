package com.demo.csjbot.csjsdkdemo.bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class FAVbean implements Serializable {
    private int mediaId;
    private String mediaName;
    private String mediaDescription;
    private String mediaPath;
    private String mediaFile;
    private String mediaType;
    private long mediaDate;

    public FAVbean(int mediaId, String mediaName, String mediaDescription, String mediaPath, String mediaFile, String mediaType, long mediaDate) {
        this.mediaId = mediaId;
        this.mediaName = mediaName;
        this.mediaDescription = mediaDescription;
        this.mediaPath = mediaPath;
        this.mediaFile = mediaFile;
        this.mediaType = mediaType;
        this.mediaDate = mediaDate;
    }

    public int getMediaId() {
        return mediaId;
    }

    public void setMediaId(int mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    public String getMediaDescription() {
        return mediaDescription;
    }

    public void setMediaDescription(String mediaDescription) {
        this.mediaDescription = mediaDescription;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getMediaFile() {
        return mediaFile;
    }

    public void setMediaFile(String mediaFile) {
        this.mediaFile = mediaFile;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public long getMediaDate() {
        return mediaDate;
    }

    public void setMediaDate(long mediaDate) {
        this.mediaDate = mediaDate;
    }
}
