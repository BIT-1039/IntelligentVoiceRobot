package com.demo.csjbot.csjsdkdemo.network;

import com.demo.csjbot.csjsdkdemo.bean.FAVbean;

import java.util.List;

import rx.Observable;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface VideoService {
    /*
     *EmergencyInquirySystem/video/getVideoList?type=xx&search=xx&start=xx&num=xxx
     */
    @GET("EmergencyInquirySystem/video/getVideoList")
    Observable<List<FAVbean>> getVideoList(@Query("type") String type, @Query("search") String search, @Query("start") int start, @Query("num") int num);
}
