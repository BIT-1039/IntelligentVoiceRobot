package com.demo.csjbot.csjsdkdemo.ui;

import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.base.MyApplication;
import com.demo.csjbot.csjsdkdemo.sdkpack.Voice;
import com.demo.csjbot.csjsdkdemo.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Author: Ge Chen
 * Date: 2019/10/28 15:23
 * Description:Splash
 */
public class SplashActivity extends AppCompatActivity {
    @BindView(R.id.icon_ninth)
    RelativeLayout settings;
    @BindView(R.id.icon_first)
    RelativeLayout yunfan;
    @BindView(R.id.icon_third)
    RelativeLayout csj;
    @BindView(R.id.webp)
    SimpleDraweeView webp;
    @BindView(R.id.icon_fifth)
    RelativeLayout intelligentQA;
    @BindView(R.id.icon_eighth)
    RelativeLayout mp4;
    @BindView(R.id.icon_seventh)
    RelativeLayout firstAidVideo;
    @BindView(R.id.icon_second)
    RelativeLayout eighth;

    @BindView(R.id.icon_sixth)
    RelativeLayout intelligentQA_2;

    @BindView(R.id.icon_fourth)
    RelativeLayout doctor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        initListener();
    }

    private void initListener() {
        yunfan.setOnClickListener(view -> startActivity(new Intent(SplashActivity.this, FirstAidKnowledgeActivity.class)));
        csj.setOnClickListener(view -> {
            MyApplication.SDK_TYPE = 1;
            Voice.speak(SplashActivity.this, "我是穿山甲机器人，我也难，都难，难点好啊！");
        });
        settings.setOnClickListener(view -> startActivity(new Intent(SplashActivity.this, SettingActivity.class)));
        final DraweeController draweeController = Fresco.newDraweeControllerBuilder().setUri(UriUtil.getUri(SplashActivity.this, R.drawable.yunfan_mascot_joking_00030))
                .setControllerListener(controllerListener).build();
        webp.setController(draweeController);
        intelligentQA.setOnClickListener(view -> startActivity(new Intent(SplashActivity.this, IntelligentQAActivity.class).putExtra("type", 1)));
        mp4.setOnClickListener(view -> startActivity(new Intent(SplashActivity.this, VideoActivity.class)));
        firstAidVideo.setOnClickListener(view -> startActivity(new Intent(SplashActivity.this, FirstAidVideoActivity.class)));
        eighth.setOnClickListener(view -> {
            //startActivity(new Intent(SplashActivity.this, TestActivity.class));
            startActivity(new Intent(SplashActivity.this, KnowledgeActivity.class));
        });
        intelligentQA_2.setOnClickListener(view->{
            startActivity(new Intent(SplashActivity.this, IntelligentQAActivity.class).putExtra("type", 2));
        });
        doctor.setOnClickListener(view->{
            startActivity(new Intent(SplashActivity.this, DoctorActivity.class));
        });
    }

    ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
        @Override
        public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable animatable) {
            super.onFinalImageSet(id, imageInfo, animatable);
            if (animatable != null) {
                animatable.start();
            }
        }
    };
}
