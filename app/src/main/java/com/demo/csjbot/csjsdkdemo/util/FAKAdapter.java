package com.demo.csjbot.csjsdkdemo.util;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.bean.FAKbean;

import java.util.ArrayList;

public class FAKAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<FAKbean> data;

    private final static int TYPE_CONTENT = 0;//正常内容
    private final static int TYPE_FOOTER = 1;//下拉刷新

    private boolean hasMore = true;   // 是否有待加载数据

    public FAKAdapter(Context context, ArrayList<FAKbean> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_FOOTER) {//底部布局
            View view = LayoutInflater.from(context).inflate(R.layout.item_footer, parent, false);
            return new FAVFooterViewHolder(view);
        } else {//正常布局
            View view = LayoutInflater.from(context).inflate(R.layout.item_rv2, parent, false);
            return new FAKViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_FOOTER) {
            ((FAVFooterViewHolder) holder).txtFooter.setVisibility(View.VISIBLE);
            if (hasMore == true) {//有数据 可以加载
                if (data.size() > 0) {
                    ((FAVFooterViewHolder) holder).txtFooter.setText("正在加载更多...");
                }
            } else {
                if (data.size() > 0) {
                    ((FAVFooterViewHolder) holder).txtFooter.setText("没有更多数据了");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((FAVFooterViewHolder) holder).txtFooter.setVisibility(View.GONE);
                            hasMore = true;
                        }
                    }, 500);
                }
            }
        } else {
            ((FAKViewHolder) holder).title.setText(data.get(position).getTitle());
            ((FAKViewHolder) holder).introduction.setText(data.get(position).getIntroduction());
            ((FAKViewHolder) holder).imgMore.setImageResource(R.drawable.more);
            ((FAKViewHolder) holder).imgMore.setOnClickListener(new View.OnClickListener() {//itemview.setOnClickListener
                @Override
                public void onClick(View v) { //点击播放
                    if (listener != null) {
                        listener.onClick(position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 1;//多了一个footer
    }

    public int getRealLastPosition() {
        return data.size(); //不算footer
    }

    private class FAKViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView introduction;
        private ImageView imgMore;

        public FAKViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt);
            imgMore = itemView.findViewById(R.id.img_more);
            introduction = itemView.findViewById(R.id.introduction);
        }
    }

    private class FAVFooterViewHolder extends RecyclerView.ViewHolder {

        private TextView txtFooter;

        public FAVFooterViewHolder(View itemView) {
            super(itemView);
            txtFooter = itemView.findViewById(R.id.txt_footer);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == data.size()) {
            return TYPE_FOOTER;
        }
        return TYPE_CONTENT;
    }

    public void resetDatas() {
        data = new ArrayList<>();
    }

    public void updateList(ArrayList<FAKbean> newDatas, boolean hasMore) {
        if (newDatas != null) {
            data.addAll(newDatas);
        }
        this.hasMore = hasMore;
        notifyDataSetChanged();
    }

    //点击事件
    public interface OnItemClickListener {
        void onClick(int position);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
