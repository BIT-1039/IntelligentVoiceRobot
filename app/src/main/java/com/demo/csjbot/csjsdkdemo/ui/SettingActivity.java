package com.demo.csjbot.csjsdkdemo.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.csjbot.csjsdkdemo.R;
import com.demo.csjbot.csjsdkdemo.base.MyApplication;
import com.demo.csjbot.csjsdkdemo.sdkpack.Voice;
import com.robot.performlib.action.AIUIAction;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Author: Ge Chen
 * Date: 2019/10/28 13:40
 * Description:
 */
public class SettingActivity extends AppCompatActivity {

    @BindView(R.id.intonation)
    SeekBar intonation;
    @BindView(R.id.intonationTxt)
    TextView intonationTxt;
    @BindView(R.id.talkingSpeed)
    SeekBar talkingSpeed;
    @BindView(R.id.talkingSpeedTxt)
    TextView talkingSpeedTxt;
    @BindView(R.id.voice_group)
    RadioGroup voiceGroup;
    @BindView(R.id.sdk_group)
    RadioGroup sdkGroup;
    @BindView(R.id.sdk_confirm)
    Button sdkConfirm;
    @BindView(R.id.voice_confirm)
    Button voiceConfirm;

    private Handler uiHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            intonationTxt.setText(String.valueOf(intonation.getProgress()));
            talkingSpeedTxt.setText(String.valueOf(talkingSpeed.getProgress()));
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        initListener();
    }

    private void initListener() {
        sdkConfirm.setOnClickListener(view -> changeSDK());
        voiceConfirm.setOnClickListener(view -> changeVoice());
        intonation.setOnSeekBarChangeListener(seekBarListener);
        talkingSpeed.setOnSeekBarChangeListener(seekBarListener);
    }

    private void changeVoice() {
        int checkId = voiceGroup.getCheckedRadioButtonId();
        int Int_intonation = intonation.getProgress();
        int Int_speed = talkingSpeed.getProgress();
        switch (checkId) {
            case R.id.radio_mengmeng:
                Voice.changeVoicer(SettingActivity.this, AIUIAction.Voicer.mengmeng, Int_speed, Int_intonation);
                break;
            case R.id.radio_xiaofang:
                Voice.changeVoicer(SettingActivity.this, AIUIAction.Voicer.xiaofang, Int_speed, Int_intonation);
                break;
            case R.id.radio_xiaofeng:
                Voice.changeVoicer(SettingActivity.this, AIUIAction.Voicer.xiaofeng, Int_speed, Int_intonation);
                break;
            case R.id.radio_xiaohou:
                Voice.changeVoicer(SettingActivity.this, AIUIAction.Voicer.xiaohou, Int_speed, Int_intonation);
                break;
            case R.id.radio_xiaotong:
                Voice.changeVoicer(SettingActivity.this, AIUIAction.Voicer.xiaotong, Int_speed, Int_intonation);
                break;
            case R.id.radio_xiaoxue:
                Voice.changeVoicer(SettingActivity.this, AIUIAction.Voicer.xiaoxue, Int_speed, Int_intonation);
                break;
            case R.id.radio_xiaoyan:
                Voice.changeVoicer(SettingActivity.this, AIUIAction.Voicer.xiaoyan, Int_speed, Int_intonation);
                break;
            case R.id.radio_yueyu:
                Voice.changeVoicer(SettingActivity.this, AIUIAction.Voicer.xiaomei, Int_speed, Int_intonation);
                break;
        }
    }

    private void changeSDK() {
        int checkId = sdkGroup.getCheckedRadioButtonId();
        switch (checkId) {
            case R.id.radio_csj:
                MyApplication.SDK_TYPE = 1;
                Toast.makeText(SettingActivity.this, "您已选择穿山甲SDK", Toast.LENGTH_SHORT).show();
                break;
            case R.id.radio_yunji:
                MyApplication.SDK_TYPE = 0;
                Toast.makeText(SettingActivity.this, "您已选择云帆SDK", Toast.LENGTH_SHORT).show();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + checkId);
        }
    }

    private SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            uiHandler.sendEmptyMessage(0);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
}

